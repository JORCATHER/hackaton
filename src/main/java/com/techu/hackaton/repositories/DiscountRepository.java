package com.techu.hackaton.repositories;

import com.techu.hackaton.HackatonApplication;
import com.techu.hackaton.models.DiscountModel;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public class DiscountRepository {

    public Optional<DiscountModel> findById(String id) {
        System.out.println("findById en DiscountRepository");

        Optional<DiscountModel> result = Optional.empty();

        //Búsqueda del descuento por id
        for (DiscountModel discount : HackatonApplication.discountModels) {
            if (discount.getId().equals(id)) {
                System.out.println("Descuento encontrado, id: " + id);
                result = Optional.of(discount);
            }
        }

        return result;
    }

    public Optional<DiscountModel> update(DiscountModel discount, String id) {
        System.out.println("update en DiscountRepository");
        System.out.println("La id del discount a actualizar es " + id);

        Optional<DiscountModel> result = this.findById(id);

        if (result.isPresent()) {
            result.get().setActive(discount.isActive());
            //result.get().setDesc(discount.getDesc());
            //result.get().setDiscount(discount.getDiscount());
            System.out.println("Descuento actualizado en DiscountRepository: " + discount);
        }

        return result;
    }

}
