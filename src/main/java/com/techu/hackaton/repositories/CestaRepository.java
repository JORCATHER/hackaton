package com.techu.hackaton.repositories;

import com.techu.hackaton.HackatonApplication;
import com.techu.hackaton.models.CestaModel;
import com.techu.hackaton.models.ProductModel;
import org.springframework.stereotype.Repository;


import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Repository
public class CestaRepository {

    public List<CestaModel> findAll() {

        System.out.println("findAll en CestaRepository");

        return HackatonApplication.cestaModels;
    }

    public CestaModel save(CestaModel cesta) {
        System.out.println("save en CestaRepository");

        HackatonApplication.cestaModels.add(cesta);

        return cesta;
    }

    public Optional<CestaModel> findById(int id) {
        System.out.println("findById en CestaRepository");

        Optional<CestaModel> result = Optional.empty();

        //Búsqueda de la cesta por id
        for (CestaModel cesta : HackatonApplication.cestaModels) {
            if (cesta.getId() == id) {
                System.out.println("Producto encontrado, id: " + id);
                result = Optional.of(cesta);
            }
        }

        return result;
    }
}
