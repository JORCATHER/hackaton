package com.techu.hackaton.repositories;

import com.techu.hackaton.HackatonApplication;
import com.techu.hackaton.models.ProductModel;
import com.techu.hackaton.services.FileServices;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public class ProductRepository {

    @Autowired
    FileServices fileServices;

    public List<ProductModel> findAll() {

        System.out.println("findAll en ProductRepository");
        this.fileServices.cargaProducts();

        return HackatonApplication.productModels;
    }

    public ProductModel save(ProductModel product) {
        System.out.println("save en ProductRepository");
        this.fileServices.cargaProducts();

        HackatonApplication.productModels.add(product);

        this.fileServices.guardaProducts();

        return product;
    }

    public Optional<ProductModel> findById(String id) {
        System.out.println("findById en ProductRepository");
        this.fileServices.cargaProducts();

        Optional<ProductModel> result = Optional.empty();

        //Búsqueda del producto por id
        for (ProductModel product : HackatonApplication.productModels) {
            if (product.getId().equals(id)) {
                System.out.println("Producto encontrado, id: " + id);
                result = Optional.of(product);
            }
        }

        return result;
    }

    public Optional<ProductModel> update(ProductModel product, String id) {
        System.out.println("update en ProductRepository");
        System.out.println("La id del producto a actualizar es " + id);
        System.out.println("El producto a actualizar es: " + product);

        Optional<ProductModel> result = this.findById(id);

        if (result.isPresent()) {
            result.get().setName(product.getName());
            result.get().setDesc(product.getDesc());
            result.get().setPrice(product.getPrice());
            result.get().setCant(product.getCant());
            System.out.println("Producto actualizado en ProductRepository: " + product);
        }

        this.fileServices.guardaProducts();
        return result;
    }

    public Optional<ProductModel> delete(String id) {
        System.out.println("delete en ProductRepository");
        System.out.println("La id del producto a borrar es " + id);

        Optional<ProductModel> result = this.findById(id);

        if (result.isPresent()) {
            System.out.println("producto borrado en ProductRepository: " + result.get());
            HackatonApplication.productModels.remove(result.get());
        }

        this.fileServices.guardaProducts();
        return result;
    }
}