package com.techu.hackaton;

import com.techu.hackaton.models.CestaModel;
import com.techu.hackaton.models.DiscountModel;
import com.techu.hackaton.models.ProductModel;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.util.ArrayList;

@SpringBootApplication
public class HackatonApplication {

	public static ArrayList<ProductModel> productModels = new ArrayList<>();
	public static ArrayList<ProductModel> productMdls = new ArrayList<>();
	public static ArrayList<CestaModel> cestaModels;
	public static ArrayList<DiscountModel> discountModels;

	public static void main(String[] args) {

		SpringApplication.run(HackatonApplication.class, args);

		HackatonApplication.productMdls = HackatonApplication.getTestData();
		HackatonApplication.cestaModels=HackatonApplication.getTestDataC();
		HackatonApplication.discountModels=HackatonApplication.getTestDataDiscounts();
	}

	private static ArrayList<ProductModel> getTestData() {
		ArrayList<ProductModel> productModels = new ArrayList<>();

		productModels.add(
				new ProductModel(
						"1"
						, "Naranjas"
						, "Qué buenas!!, tan ricas y tan naranjas"
						, 1
						,1
				)
		);

		productModels.add(
				new ProductModel(
						"2"
						, "Limones"
						, "El limón, el limonero, el que quiere el mundo entero!!!"
						, 2
						, 1
				)
		);

		productModels.add(
				new ProductModel(
						"3"
						, "Malacatones"
						, "Melocotón, que me mola un montón y me lo traen en el furgón!!!"
						, 4
						, 1
				)
		);

		return productModels;
	}

	private static ArrayList<CestaModel> getTestDataC(){
		ArrayList<CestaModel> cesta = new ArrayList<>();

		CestaModel pedido = new CestaModel();
		pedido.setId(1);
		pedido.setProductos(productMdls);
		cesta.add(pedido);

		ArrayList<ProductModel> productsCesta2 = new ArrayList<>();

		productsCesta2.add(
				new ProductModel(
						"2"
						, "Limones"
						, "El limón, el limonero, el que quiere el mundo entero!!!"
						, 2
						,1
				)
		);
		productsCesta2.add(
				new ProductModel(
						"2"
						, "Peras"
						, "Peras limoneras, las mejores mientras esperas!!"
						, 7
						,1
				)
		);
		CestaModel pedido2 = new CestaModel();
		pedido2.setId(2);
		pedido2.setProductos(productsCesta2);
		cesta.add(pedido2);

		return cesta;
	}

	private static ArrayList<DiscountModel> getTestDataDiscounts() {
		ArrayList<DiscountModel> discountModels = new ArrayList<>();

		discountModels.add(
				new DiscountModel(
						"1",
						false,
						"Black Friday",
						25
						)
		);

		return discountModels;
	}
}

