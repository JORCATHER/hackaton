package com.techu.hackaton.controllers;

import com.techu.hackaton.models.DiscountModel;
import com.techu.hackaton.models.ProductModel;
import com.techu.hackaton.services.DiscountService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Optional;

@RestController
@RequestMapping("/hackaton/discounts")
//CORS
@CrossOrigin(origins = "*", methods = {RequestMethod.GET, RequestMethod.POST, RequestMethod.DELETE, RequestMethod.PUT})
public class DiscountController {

    @Autowired
    DiscountService discountService;

    @GetMapping("/{id}")
    public ResponseEntity<Object> getDiscountById(@PathVariable String id) {
        System.out.println("getDiscountById");
        System.out.println("La id del descuento a buscar es: " + id);

        Optional<DiscountModel> result = this.discountService.findById(id);

        return new ResponseEntity<>(
                result.isPresent() ? result.get() : "Descuento no encontrado",
                result.isPresent() ? HttpStatus.OK : HttpStatus.NOT_FOUND
        );
    }

    @PutMapping("/{id}")
    public ResponseEntity<Object> updateDiscount(@RequestBody DiscountModel discount, @PathVariable String id) {
        System.out.println("updateDiscount");
        System.out.println("La id del discount a actualizar es: " + id);
        System.out.println("El descuento a actualizar es: " + discount);

        Optional<DiscountModel> result = this.discountService.updateDiscount(discount, id);

        return new ResponseEntity<>(
                result.isPresent() ? result.get() : "Descuento no encontrado",
                result.isPresent() ? HttpStatus.OK : HttpStatus.NOT_FOUND
        );
    }
}
