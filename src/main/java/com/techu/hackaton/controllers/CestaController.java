package com.techu.hackaton.controllers;

import com.techu.hackaton.models.CestaModel;
import com.techu.hackaton.models.ProductModel;
import com.techu.hackaton.services.CestaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/hackaton/cestas")
//CORS
@CrossOrigin(origins = "*", methods = {RequestMethod.GET, RequestMethod.POST, RequestMethod.DELETE, RequestMethod.PUT})
public class CestaController {

    @Autowired
    CestaService cestaService;

    @GetMapping("")
    public ResponseEntity<List<CestaModel>> getCestas() {
        System.out.println("getCestas");

        return new ResponseEntity<>(
                this.cestaService.findAll(),
                HttpStatus.OK
        );
    }

    @PostMapping("")
    public ResponseEntity<CestaModel> newCesta(@RequestBody CestaModel cesta) {
        System.out.println("newCesta");
        System.out.println("La cesta nueva es: " + cesta);

        return new ResponseEntity<>(
                this.cestaService.add(cesta),
                HttpStatus.CREATED
        );
    }

    @GetMapping("/{id}")
    public ResponseEntity<Object> getCestaById(@PathVariable String id) {
        System.out.println("getCestaById");
        System.out.println("La id de la cesta a buscar es: " + id);

        Optional<CestaModel> result = this.cestaService.findById(id);

        return new ResponseEntity<>(
                result.isPresent() ? result.get() : "Cesta no encontrada",
                result.isPresent() ? HttpStatus.OK : HttpStatus.NOT_FOUND
        );
    }

}
