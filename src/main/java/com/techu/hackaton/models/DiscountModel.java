package com.techu.hackaton.models;

import java.util.ArrayList;

public class DiscountModel {

    private String id;
    private boolean active;
    private String desc;
    private double discount;   //descuento en porcentaje

    public DiscountModel() {
    }

    public DiscountModel(String id, boolean active, String desc, double discount) {
        this.id = id;
        this.active = active;
        this.desc = desc;
        this.discount = discount;
    }

    public String getId() {
        return this.id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public boolean isActive() {
        return this.active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }

    public String getDesc() {
        return this.desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    public double getDiscount() {
        return this.discount;
    }

    public void setDiscount(double discount) {
        this.discount = discount;
    }

    @Override
    public String toString() {
        return "DiscountModel{" +
                "id='" + id + '\'' +
                ", active=" + active +
                ", desc='" + desc + '\'' +
                ", discount=" + discount +
                '}';
    }
}
