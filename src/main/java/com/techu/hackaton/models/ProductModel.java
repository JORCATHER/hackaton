package com.techu.hackaton.models;

import java.io.Serializable;

public class ProductModel implements Serializable {
    private String id;
    private String name;
    private String desc;
    private double price;
    private int cant;

    public ProductModel() {
    }

    public ProductModel(String id, String name, String desc, double price, int cant) {
        this.id = id;
        this.name = name;
        this.desc = desc;
        this.price = price;
        this.cant = cant;
    }

    public String getId() {
        return this.id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return this.name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDesc() {
        return this.desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    public double getPrice() {
        return this.price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public int getCant() {
        return this.cant;
    }

    public void setCant(int cant) {
        this.cant = cant;
    }

    @Override
    public String toString() {
        return "ProductModel{" +
                "id='" + id + '\'' +
                ", name='" + name + '\'' +
                ", desc='" + desc + '\'' +
                ", price=" + price +
                ", cant=" + cant +
                '}';
    }
}
