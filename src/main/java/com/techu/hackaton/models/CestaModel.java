package com.techu.hackaton.models;

import java.util.ArrayList;

public class CestaModel {

    private int id;
    private ArrayList<ProductModel> productos;

    public CestaModel(int id, ArrayList<ProductModel> productos) {
        this.id = id;
        this.productos = productos;
    }

    public CestaModel() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public ArrayList<ProductModel> getProductos() {
        return productos;
    }

    public void setProductos(ArrayList<ProductModel> cesta) {
        this.productos = cesta;
    }
}
