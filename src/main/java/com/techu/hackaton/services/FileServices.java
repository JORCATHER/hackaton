package com.techu.hackaton.services;

import com.techu.hackaton.HackatonApplication;
import com.techu.hackaton.models.ProductModel;
import org.springframework.stereotype.Service;

import java.io.*;
import java.util.ArrayList;

@Service
public class FileServices {

    public String guardaProducts () {

        System.out.println("Guardando Products a fichero ");

        try {
            File file = new File(".\\Files\\Products.dat");
            FileOutputStream fichero = new FileOutputStream(".\\Files\\Products.dat");

            ObjectOutputStream escribiendoFichero = new ObjectOutputStream(fichero);

            escribiendoFichero.writeObject(HackatonApplication.productModels);
            escribiendoFichero.close();

            System.out.println("Escritura de Products a fichero OK!");

        } catch (Exception e) {
            System.out.println("Error al escribir Products a fichero: " + e.getMessage());
        }

        return "OK";
    }


    public String cargaProducts (){
        try {
            ObjectInputStream leyendoFichero = new ObjectInputStream(
                    new FileInputStream(".\\Files\\Products.dat"));

            HackatonApplication.productModels.clear();
            HackatonApplication.productModels = ( ArrayList <ProductModel> )leyendoFichero.readObject();

            leyendoFichero.close();

            System.out.println("Lectura correcta de products.dat " + HackatonApplication.productModels);

        } catch (FileNotFoundException e) {
            System.out.println("No encuentro products.dat " + e.getMessage());
        } catch (EOFException e) {
            System.out.println("Fin de fichero de Products.dat");
        } catch (IOException e) {
            System.out.println("Error de lectura de Products.dat" + e.getMessage());
        } catch (ClassNotFoundException e) {
            System.out.println("Error Class not found de Products.dat " + e.getMessage());
        }

        return "OK";
    }

}
