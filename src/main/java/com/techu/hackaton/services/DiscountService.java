package com.techu.hackaton.services;

import com.techu.hackaton.models.DiscountModel;
import com.techu.hackaton.models.ProductModel;
import com.techu.hackaton.repositories.DiscountRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class DiscountService {

    @Autowired
    DiscountRepository discountRepository;

    public Optional<DiscountModel> findById(String id) {
        System.out.println("findById en DiscountService");

        return this.discountRepository.findById(id);
    }

    public Optional<DiscountModel> updateDiscount(DiscountModel product, String id) {
        System.out.println("updateDiscount en DiscountService");

        return this.discountRepository.update(product, id);
    }
}
