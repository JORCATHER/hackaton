package com.techu.hackaton.services;

import com.techu.hackaton.models.ProductModel;
import com.techu.hackaton.repositories.ProductRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

import static com.techu.hackaton.HackatonApplication.productModels;

@Service
public class ProductService {
    @Autowired
    ProductRepository productRepository;
    @Autowired
    DiscountService discountService;

    public List<ProductModel> findAll() {
        System.out.println("findAll en ProductService");
        List<ProductModel> result = this.productRepository.findAll();

        //Aplicamos descuento 1 por Black Friday, en un futuro buscaríamos si hay más promociones
        if (discountService.findById("1").isPresent()){
            if (discountService.findById("1").get().isActive()) {
                double discount = discountService.findById("1").get().getDiscount();
                for (ProductModel product : result) {
                    product.setPrice(product.getPrice() / 100 * (100 - discount));
                }
            }
        }

        return result;
    }

    public ProductModel add(ProductModel product) {
        System.out.println("add en ProductService");

        int id=Integer.parseInt(productModels.get(productModels.size()-1).getId());
        id += 1;
        product.setId(id+"");

        return this.productRepository.save(product);
    }

    public Optional<ProductModel> findById(String id) {
        System.out.println("findById en ProductService");

        return this.productRepository.findById(id);
    }

    public Optional<ProductModel> updateProduct(ProductModel product, String id) {
        System.out.println("updateProduct en ProductService");

        return this.productRepository.update(product, id);
    }

    public Optional<ProductModel> delete(String id) {
        System.out.println("delete en ProductService");

        return this.productRepository.delete(id);
    }
}
