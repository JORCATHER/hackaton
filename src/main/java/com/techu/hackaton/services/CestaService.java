package com.techu.hackaton.services;

import com.techu.hackaton.HackatonApplication;
import com.techu.hackaton.models.CestaModel;
import com.techu.hackaton.models.ProductModel;
import com.techu.hackaton.repositories.CestaRepository;
import com.techu.hackaton.repositories.ProductRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import static com.techu.hackaton.HackatonApplication.cestaModels;
import static com.techu.hackaton.HackatonApplication.productModels;

@Service
public class CestaService {

    @Autowired
    CestaRepository cestaRepository;
    @Autowired
    ProductService productService;
    @Autowired
    FileServices fileServices;

    public List<CestaModel> findAll() {
        System.out.println("findAll en CestaService");

        return this.cestaRepository.findAll();
    }

    public CestaModel add(CestaModel cesta) {
        System.out.println("add en CestaService: " + cesta);

        if (cestaModels.size() == 0) {
            cesta.setId(1);
        } else {
            int id = cestaModels.get(cestaModels.size() - 1).getId();
            id += 1;
            cesta.setId(id);
        }

        this.actuCant(cesta);
        this.fileServices.guardaProducts();

        return this.cestaRepository.save(cesta);
    }

    public void actuCant(CestaModel cesta) {
        System.out.println("Actualizamos cantidad en productos");

        ArrayList<ProductModel> productos = cesta.getProductos();

        for (int j=0;j<productos.size();j++) {

            int cantV= HackatonApplication.productModels.get(j).getCant();
            int cant=productos.get(j).getCant();

            if (cantV-cant<=0){
                HackatonApplication.productModels.get(j).setCant(0);
            }

            HackatonApplication.productModels.get(j).setCant(cantV-cant);

        }
    }

    public Optional<CestaModel> findById(String id) {
        System.out.println("findById en CestaService");

        int id2 = 0;

        if (id.chars().allMatch(Character::isDigit)) {
            id2 = Integer.parseInt(id);
        }

        return this.cestaRepository.findById(id2);
    }

    public void calculaPrecio(CestaModel cesta) {

        System.out.println("calculaPrecio en CestaService");
        ArrayList<ProductModel> productos = cesta.getProductos();

        double precioTotal = 0;

        for (ProductModel productoCesta : productos) {
               Optional<ProductModel> stockProduct = this.productService.findById(productoCesta.getId());

               if (stockProduct.isPresent()) {
                   precioTotal += (stockProduct.get().getPrice() * productoCesta.getCant());
               }
        }

        // faltaría añadir el precio total de la cesta al CestaModel
    }

}
